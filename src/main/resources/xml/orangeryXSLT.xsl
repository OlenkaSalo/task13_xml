<?xml version="1.0" encoding="UTF-8" ?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table  {
                    border-color:#df1414;
                    background:#79ede9;
                    border-collapse:collapse;
                    text-align:center;
                    }
                    table, td, th {
                    border-color:#4f1ae9;
                    border:2px solid black;
                    border-width:2px;
                    text-align:center;
                    }
                </style>
            </head>
            <body>
                <table >
                   <tr>
                       <th >Name</th>
                       <th >Soil</th>
                       <th >Origin</th>
                       <th >Multiplying</th>
                       <th >VisualParametrs</th>
                       <th>Temperature</th>
                       <th >Luminosity</th>
                       <th >Watering</th>
                   </tr>
                    <xsl:for-each select="Flowers/flower">
                        <tr>
                            <td ><xsl:value-of select="name" /></td>
                            <td ><xsl:value-of select="soil" /></td>
                            <td ><xsl:value-of select="origin" /></td>
                            <td ><xsl:value-of select="multiplying" /></td>
                        <td >
                        <xsl:for-each select="VisualParameters/visualParametrs">

                                <xsl:value-of select= "."/>
                            <br/>

                                </xsl:for-each>
                        </td>

                            <td ><xsl:value-of select="growingTips/temperature" /></td>
                            <td ><xsl:value-of select="growingTips/luminosity" /></td>
                            <td ><xsl:value-of select="growingTips/watering" /></td>

                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:transform>

