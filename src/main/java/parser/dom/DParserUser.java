package parser.dom;

import org.w3c.dom.Document;
import xmlfilling.Flower;

import java.io.File;
import java.util.List;

public class DParserUser {
    public static List<Flower> getFlowerList(File xml, File xsd ){
        DDocCreator creator = new DDocCreator(xml);
        Document doc= creator.getDoc();
        DDocReader reader = new DDocReader();
        return reader.resdDoc(doc);
    }
}
