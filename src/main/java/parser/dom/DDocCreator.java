package parser.dom;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class DDocCreator {
    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder builder;
    private Document doc;

    public  DDocCreator(File xml){
        creatorDBuilder();
        creatorDoc(xml);
    }

    private void creatorDBuilder()
    {
        builderFactory = DocumentBuilderFactory.newInstance();
        try{
            builder=builderFactory.newDocumentBuilder();

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void creatorDoc(File xml){
        try{
            doc= builder.parse(xml);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Document getDoc(){return doc;}
}
