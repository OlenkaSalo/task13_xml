package parser.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xmlfilling.Flower;
import xmlfilling.GrowingTips;
import xmlfilling.VisualParameters;

import java.util.ArrayList;
import java.util.List;

public class DDocReader {
    public List<Flower>resdDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Flower> flowers = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("flower");

        for(int i = 0;i<nodeList.getLength(); i++)
        {
            Flower flower = new Flower();
            GrowingTips growingTips;
            List<VisualParameters> parametersList;

            Node node=nodeList.item(i);
            if(node.getNodeType()==Node.ELEMENT_NODE){
                Element element = (Element)node;

                flower.setName(element.getElementsByTagName("name").item(0).getTextContent());
                flower.setSoil(element.getElementsByTagName("soil").item(0).getTextContent());
                flower.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
                flower.setMultiplying(element.getElementsByTagName("multiplying").item(0).getTextContent());

                parametersList=getParameters(element.getElementsByTagName("VisualParameters"));
                growingTips  = getGrowingTips(element.getElementsByTagName("growingTips"));

                flower.setListParameters(parametersList);
                flower.setGrowingTips(growingTips);
                flowers.add(flower);

            }
        }
        return flowers;
    }

    private List<VisualParameters> getParameters(NodeList nodes){
        List<VisualParameters> parametersList = new ArrayList<>();
        if(nodes.item(0).getNodeType()== Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for(int i = 0; i<nodeList.getLength(); i++){
                Node node= nodeList.item(i);
                if(node.getNodeType()==Node.ELEMENT_NODE){
                    Element element1 = (Element) node;
                    parametersList.add(new VisualParameters(element1.getTextContent()));
                }
            }
        }
        return parametersList;
    }

    private GrowingTips getGrowingTips(NodeList nodes){
        GrowingTips growingTips = new GrowingTips();
        if(nodes.item(0).getNodeType()==Node.ELEMENT_NODE){
            Element element = (Element)nodes.item(0);
            growingTips.setTemperature(Double.parseDouble(element.getElementsByTagName("temperature").item(0).getTextContent()));
            growingTips.setLuminosity(Boolean.parseBoolean(element.getElementsByTagName("luminosity").item(0).getTextContent()));
            growingTips.setWatering(Integer.parseInt(element.getElementsByTagName("watering").item(0).getTextContent()));
        }
        return growingTips;
    }
}
