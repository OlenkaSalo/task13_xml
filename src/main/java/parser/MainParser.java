package parser;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import comparator.WateringComparator;
import existencefile.FileInfo;
import existencefile.FileInfo.*;
import parser.dom.DParserUser;
import parser.sax.SParserUser;
import parser.stax.SxReader;
import xmlfilling.Flower;

public class MainParser {
    public static void main(String[] args) {
        File xml = new File("src\\main\\resources\\xml\\orangeryXML.xml");
        File xsd = new File("src\\main\\resources\\xml\\orangeryXSD.xsd");

        if (checkIfXML(xml) && checkIfXSD(xsd)) {
            printList(DParserUser.getFlowerList(xml,xsd),"DOM");
            printList(SParserUser.parseFlowers(xml,xsd), "SAX");
            printList(SxReader.parserFlower(xml,xsd),"StAX");

        }
    }

        private static boolean checkIfXML(File xml){return FileInfo.isXML(xml);}

        private static boolean checkIfXSD(File xsd){return FileInfo.isXSD(xsd);}

        private static void printList(List<Flower> flowers, String parserName){
            Collections.sort(flowers, new WateringComparator());
            System.out.println(parserName);

            for (Flower f: flowers){
                System.out.println(f);
            }
    }
}
