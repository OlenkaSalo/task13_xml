package parser.sax;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xmlfilling.Flower;
import xmlfilling.GrowingTips;
import xmlfilling.VisualParameters;

import java.util.ArrayList;
import java.util.List;

public class SHandler extends DefaultHandler {
    private List<Flower> flowerList = new ArrayList<>();
    private Flower flower=null;
    private List<VisualParameters>parametersList = null;
    private GrowingTips growingTips = null;

    private boolean b_name = false;
    private boolean b_soil = false;
    private boolean b_origin = false;
    private boolean b_multiplying = false;
    private boolean b_visualParameters = false;
    private boolean b_parameter=false;
    private boolean b_GrowingTips = false;
    private boolean b_temperature = false;
    private boolean b_luminosity = false;
    private boolean b_watering = false;

    public List<Flower> getFlowerList(){return this.flowerList;}

    public void startElem(String qName) {
        if (qName.equalsIgnoreCase("name")) {
            b_name = true;
        } else if (qName.equalsIgnoreCase("soil")) {
            b_soil = true;
        } else if (qName.equalsIgnoreCase("origin")) {
            b_origin = true;
        } else if (qName.equalsIgnoreCase("multiplying")) {
            b_multiplying = true;
        } else if (qName.equalsIgnoreCase("VisualParameters")) {
            b_visualParameters = true;
        }else if(qName.equalsIgnoreCase("parameters")){
            b_parameter = true;
        } else if (qName.equalsIgnoreCase("GrowingTips")) {
            b_GrowingTips = true;
        } else if (qName.equalsIgnoreCase("temperature")) {
            b_temperature = true;
        } else if (qName.equalsIgnoreCase("luminosity")) {
            b_luminosity = true;
        } else if (qName.equalsIgnoreCase("watering")) {
            b_watering = true;

        }
    }

    public void endElem(String qName)throws SAXException {
        if(qName.equalsIgnoreCase("flower"))
        {
            flowerList.add(flower);
        }
    }


    public void characters(char[] ch,int start, int length)
    {
        if(b_name){
            flower.setName(new String(ch,start,length));
            b_name=false;
        }
        else if(b_soil){
            flower.setSoil(new String(ch,start,length));
            b_soil=false;
        }
        else if(b_origin){
            flower.setOrigin(new String(ch,start,length));
            b_origin=false;
        }
        else if(b_multiplying){
            flower.setMultiplying(new String(ch,start,length));
            b_multiplying=false;
        }
        else if(b_visualParameters){
            parametersList = new ArrayList<>();
            b_visualParameters=false;
        }
        else if (b_parameter){
            VisualParameters visualParameters = new VisualParameters();
            visualParameters.setParameter(new String(ch,start,length));
            parametersList.add(visualParameters);
            b_parameter = false;
        }else if(b_GrowingTips){
            growingTips=new GrowingTips();
            b_GrowingTips=false;
        }else if(b_temperature){
            double temp = Double.parseDouble(new String(ch,start,length));
            growingTips.setTemperature(temp);
            b_temperature=false;
        }else if(b_luminosity){
            boolean lum = Boolean.parseBoolean(new String(ch,start,length));
            growingTips.setLuminosity(lum);
            b_luminosity=false;
        }else if(b_watering){
            int w = Integer.parseInt(new String(ch,start,length));
            growingTips.setWatering(w);
            b_watering=false;
        }

    }
}
