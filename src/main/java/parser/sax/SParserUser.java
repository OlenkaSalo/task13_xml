package parser.sax;
import org.xml.sax.SAXException;
import xmlfilling.Flower;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SParserUser {
    private static SAXParserFactory sParserFactory = SAXParserFactory.newInstance();

    public static List<Flower> parseFlowers(File xml, File xsd){
        List<Flower> flowerList = new ArrayList<>();
        try {
            sParserFactory.setSchema(SValidator.createSchema(xsd));
            SAXParser saxParser = sParserFactory.newSAXParser();
            SHandler sHandler = new SHandler();
            saxParser.parse(xml,sHandler);

            flowerList=sHandler.getFlowerList();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flowerList;
    }
}
