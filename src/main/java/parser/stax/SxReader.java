package parser.stax;

import xmlfilling.Flower;
import xmlfilling.GrowingTips;
import xmlfilling.VisualParameters;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class SxReader {
    public static List<Flower> parserFlower(File xml, File xsd){
        List<Flower> flowerList  =new ArrayList<>();
        Flower flower = null;
        GrowingTips growingTips = null;
        List<VisualParameters> visualParameters = null;

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if(xmlEvent.isStartElement()){
                    StartElement startElement =xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name){
                        case "flower":
                            flower =new Flower();
                            break;
                        case "name":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert flower!=null;
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "soil":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert  flower!=null;
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "origin":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert  flower!=null;
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "multuplying":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert  flower!=null;
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "visualParameters":
                            xmlEvent=xmlEventReader.nextEvent();
                            visualParameters = new ArrayList<>();
                            break;
                        case "parameter":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert visualParameters !=null;
                            visualParameters.add(new VisualParameters(xmlEvent.asCharacters().getData()));
                            break;
                        case "growingTips":
                            xmlEvent=xmlEventReader.nextEvent();
                            growingTips=new GrowingTips();
                            break;
                        case "temperature":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert  growingTips!=null;
                            growingTips.setTemperature(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "luminosity":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert  growingTips!=null;
                            growingTips.setLuminosity(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "watering":
                            xmlEvent=xmlEventReader.nextEvent();
                            assert  growingTips!=null;
                            growingTips.setWatering(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                    }

                    if(xmlEvent.isEndElement()){
                        EndElement endElement = xmlEvent.asEndElement();
                        if(endElement.getName().getLocalPart().equals("flowers")){
                            flowerList.add(flower);
                        }

                    }

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return flowerList;
    }
}
