package comparator;

import xmlfilling.Flower;

import java.util.Comparator;

public class WateringComparator implements Comparator<Flower> {
    public int compare(Flower f1, Flower f2) {

        return Integer.compare(f1.getGrowingTips().getWatering(),f2.getGrowingTips().getWatering());
    }
}
